#!/usr/bin/env python

"""
Spoofs a BYD to Sungrow // Per Carlen
Inspiration from: https://github.com/SunshadeCorp/can-service
"""

import os
import asyncio

#from twisted.internet.task import LoopingCall
import time
import json
import yaml
import paho.mqtt.client as paho
import copy
from struct import pack

import can

# --------------------------------------------------------------------------- #
# some global vars
# --------------------------------------------------------------------------- #
inverter_debug = 0

bms_timeout = 120
last_updated_soc = 0
status = -1
local_status = -1

def read_config_file():

    config_json = ""
    filename = "config.yaml"
    path = "/etc/batt2gen24/"
    if os.path.isfile(path+filename):
        filename = path + filename
    print("reading from:",filename)
    try:
      with open(filename,'r') as file:
        config_json = json.dumps(yaml.safe_load(file))
    except:
      print("ERROR: No config file - ",filename)
      exit(1)

    return config_json


# --------------------------------------------------------------------------- #
# configure logging
# --------------------------------------------------------------------------- #
import logging
if inverter_debug:
    logging.basicConfig()
    log = logging.getLogger()
    log.setLevel(logging.DEBUG)



def can_message(msg: can.Message) -> None:

    global bms_type
    global bms_debug
    global send_intial_data

    json_string = ""
    if msg is not None:
      if msg.arbitration_id == 337: 
        data = msg.data[0] & 0x01
        if data == 1:
          send_intial_data = True


# --------------------------------------------------------------------------- #
# thread to update the registers from mqtt data
# --------------------------------------------------------------------------- #
def update_registers():
    global logic_data
    global bms_timeout
    global inverter_debug
    global last_updated_soc
    global status
    global local_status

    #context = a[0]
    slave_id = 0x00

    # Read the current stored values in the registers
    try:
      lu = logic_data['last_updated']
    except:
      if inverter_debug: print("No good mqtt data yet")
      return

    if logic_data['status'] == 4 or local_status < 3: #FAULT or old bms data, don't change this unless bms is restarted
      time_now = int(time.time())
      time_diff = int(time.time() - last_updated_soc)
      if time_diff > bms_timeout:
        print("Refusing to update when FAULT or old BMS data")
        return

# --------------------------------------------------------------------------- #
# thread to check registers for inverter writes
# --------------------------------------------------------------------------- #
async def interval_tasks(config):
  global logic_data
  global mqtt_client1
  global last_updated_soc
  global bms_timeout
  global inverter_debug
  global last_updated_soc
  global status
  global local_status

  while 1:
    time_now = int(time.time())
    time_diff = int(time.time() - last_updated_soc)
    if inverter_debug: print("Check register - start - diff:",time_diff)

    if time_diff > bms_timeout:
      print("Old BMS data, setting status to INACTIVE", time_diff)
      local_status = 1 #INACTIVE = meaning stop sending updates on can, could be a better way
    else: 
      local_status = 3
    await asyncio.sleep(1)


def mqtt_on_publish(mqtt_client,userdata,result): 
    global inverter_debug
    if inverter_debug: print("data published:",result)
    pass

def mqtt_on_message(mqtt_client,userdata, message,tmp=None):
    global inverter_debug
    global last_updated_soc
    global logic_data

    if inverter_debug:
      print(" Received message " + str(message.payload)
        + " on topic '" + message.topic
        + "' with QoS " + str(message.qos))


    if message.topic == "inverter/data":
      if inverter_debug: print("Got mqtt update")
      json_data = json.loads(message.payload)
      if type(json_data) is dict or type(json_data) is list:
        last_updated_soc = json_data['last_updated_soc']
        status = json_data['status']
        logic_data = json_data
      update_registers()

def mqtt_on_subscribe(client, userdata, mid, granted_qos):
    pass

def mqtt_on_connect(client, userdata, flags, rc):
    if rc != 0:
      print("MQTT connection problem")
      client.connected_flag=False
    else:
      print("MQTT client connected:" + str(client))
      client.connected_flag=True

def publish(client,msg_string,topic):
    global inverter_debug
    #print("Publishing in",topic,msg_string)
    ret = client.publish(topic,msg_string) 


def run_updating_server(class_InverterHandler):

    global InverterHandler
    global inverter_type
    global inverter_data
    global mqtt_client1
    global mqtt_client2
    global logic_data
    global inverter_debug
    global bms_timeout
    global status

    InverterHandler = class_InverterHandler
    inverter_config = InverterHandler.config
    inverter_debug = InverterHandler.config['inverter_debug']
    print("\nStarting inverter module for ",InverterHandler.config['inverter_type'])
    bms_timeout = InverterHandler.config['bms_timeout']
    inverter_debug = InverterHandler.config['inverter_debug']
    inverter_type = InverterHandler.config['inverter_type']
    inverter_interface = InverterHandler.config['inverter_interface']
    mqtt_broker = InverterHandler.config['mqtt_broker']
    mqtt_port = InverterHandler.config['mqtt_port']
    mqtt_username = InverterHandler.config['mqtt_username']
    mqtt_password = InverterHandler.config['mqtt_password']

    # this mqtt client will publish
    mqtt_client1 = paho.Client(client_id="inverter_pub",transport="tcp",protocol=paho.MQTTv311,clean_session=True)  
    mqtt_client1.on_publish = mqtt_on_publish
    mqtt_client1.on_connect = mqtt_on_connect
    mqtt_client1.username_pw_set(mqtt_username, mqtt_password)
    mqtt_client1.connect(mqtt_broker,mqtt_port,keepalive=60)
    mqtt_client1.loop_start()

    # this mqtt client will subscribe
    mqtt_client2 = paho.Client(client_id="inverter_sub",transport="tcp",protocol=paho.MQTTv311,clean_session=True)  
    mqtt_client2.on_subscribe = mqtt_on_subscribe
    mqtt_client2.on_connect = mqtt_on_connect
    mqtt_client2.on_message = mqtt_on_message
    mqtt_client2.username_pw_set(mqtt_username, mqtt_password)
    mqtt_client2.connect(mqtt_broker,mqtt_port,keepalive=60)
    mqtt_client2.subscribe("inverter/data",2)
    mqtt_client2.loop_start()

    start_up = False
    while not start_up:
      try:
        lu = logic_data['last_updated']
        start_up = True
        status = 3
      except:
        if inverter_debug: print("No mqtt data yet")
        time.sleep(1)
        pass

    print("Starting can receiver")
    asyncio.run(run_async_server(InverterHandler.config), debug=True)
    print("Server ended")
    exit(1)


def send_can(bus,msg):
    rc = True
    try:
      bus.send(msg)
    except can.CanError:
      rc = False
      print(msg, "Couldn't send")
      pass
    return rc

def send_can_messages(bus,messages):
    global status
    global local_status
    return_code = False
    if status > 1 and local_status > 1 : # Don't send if status is bad
      return_code = True
      for message in messages:
        b = bytearray()
        b.extend(map(ord,message['data']))
        msg = can.Message(arbitration_id = message['id'], timestamp=time.time(),data=b, is_extended_id=False)
        rc = send_can(bus,msg)
        if not rc: return_code = False
    return return_code

def send_can_initial(bus,config,logic_data):
    messages =  [ { 'id':0x250, 'data':'\x03\x16\x00\x66\x00\x33\x02\x09' },
                  { 'id':0x290, 'data':'\x06\x37\x10\xd9\x00\x00\x00\x00' },
                  { 'id':0x2d0, 'data':'\x00' + 'BYD' + '\x00\x00\x00\x00' },
                  { 'id':0x3d0, 'data':'\x00' + 'Battery' },
                  { 'id':0x3d0, 'data':'\x01' + '-Box Pr' },
                  { 'id':0x3d0, 'data':'\x02' + 'emium H' },
                  { 'id':0x3d0, 'data':'\x03' + 'VS' + '\x00' * 5 },
                ]

    return_code = send_can_messages(bus,messages)
    return return_code

def insert_value_into_string(pos,into_string,v):
    if v > 32768: v = 32767
    if v < -32767: v = -32767
    by = int(v).to_bytes(2, byteorder = 'big',signed = 1)
    s1 = chr(by[0]) + chr(by[1])
    s = into_string[:pos] + s1 + into_string[pos + 2:]
    return s


def send_can_2sec(bus,config,logic_data):

    charge_max_current = logic_data['static_max_power'] / logic_data['static_max_voltage']
    discharge_max_current = logic_data['static_max_power'] / logic_data['static_max_voltage']
    r110 = '\x00' * 8
    r110 = insert_value_into_string(0,r110,logic_data['static_max_voltage'] * 10)
    r110 = insert_value_into_string(2,r110,logic_data['static_min_voltage'] * 10)
    r110 = insert_value_into_string(4,r110,charge_max_current * 10)
    r110 = insert_value_into_string(6,r110,discharge_max_current * 10)

    messages =  [ { 'id':0x110, 'data': r110 },
                ]
    msg = can.Message(arbitration_id=0x2d0, timestamp=time.time(),
          data=[0x03,0x16,0x00,0x66,0x00,0x33,0x02,0x09   ], is_extended_id=False)
    return_code = send_can_messages(bus,messages)
    return return_code

def send_can_10sec(bus,config,logic_data):

    batt_current = logic_data['batt_power'] / logic_data['batt_voltage']
    batt_temp = (logic_data['cell_temp_min'] + logic_data['cell_temp_max']) / 2
    target_discharge_current = logic_data['target_discharge_power'] / logic_data['static_max_voltage']
    target_charge_current = logic_data['target_charge_power'] / logic_data['static_max_voltage']
    r150 = '\x00' * 8
    r150 = insert_value_into_string(0,r150,logic_data['soc'] * 100)
    r150 = insert_value_into_string(2,r150,logic_data['soh'] * 100 )
    r150 = insert_value_into_string(4,r150,target_discharge_current * 10)
    r150 = insert_value_into_string(6,r150,target_charge_current * 10)
    r1d0 = '\x00' * 8
    r1d0 = insert_value_into_string(0,r1d0,logic_data['batt_voltage'] * 10)
    r1d0 = insert_value_into_string(2,r1d0,batt_current * 10)
    r1d0 = insert_value_into_string(4,r1d0,batt_temp * 10)
    r1d0 = insert_value_into_string(6,r1d0,776) #????
    r210 = '\x00' * 8
    r210 = insert_value_into_string(0,r210,logic_data['cell_temp_max'] * 10)
    r210 = insert_value_into_string(2,r210,logic_data['cell_temp_min'] * 10)
    r210 = insert_value_into_string(4,r210,0)
    r210 = insert_value_into_string(6,r210,0)

    messages =  [ { 'id':0x150, 'data': r150 },
                  { 'id':0x1d0, 'data': r1d0 },
                  { 'id':0x210, 'data': r210 },
                ]
    return_code = send_can_messages(bus,messages)
    return return_code

def send_can_60sec(bus,config,logic_data):

    r190 = '\x00' * 8
    r190 = insert_value_into_string(2,r190,3) #Status???
    messages =  [ { 'id':0x190, 'data': r190 },
                ]
    return_code = send_can_messages(bus,messages)
    return return_code


async def StartCAN(config):
    global send_intial_data
    global logic_data

    print("Initializing CAN Bus on ",config['inverter_interface'])
    send_intial_data = False
    with can.Bus(interface=config['inverter_interface_type'],channel=config['inverter_interface'],bitrate=config['inverter_interface_bitrate']) as bus:

        listeners: List[MessageRecipient] = [
            can_message,  # Callback function
        ]

        # Create Notifier with an explicit loop to use for scheduling of callbacks
        can_loop = asyncio.get_running_loop()
        notifier = can.Notifier(bus, listeners, loop=can_loop)

        seconds = 0

        try:
            while True:
                if send_intial_data:
                  rc = send_can_initial(bus,config,logic_data)
                  if rc: send_intial_data = False
                if seconds % 2 == 0:
                  send_can_2sec(bus,config,logic_data)
                if seconds % 10 == 0:
                  send_can_10sec(bus,config,logic_data)
                if seconds % 60 == 0:
                  send_can_60sec(bus,config,logic_data)
                  seconds = 0
                await asyncio.sleep(1)
                seconds += 1


        except KeyboardInterrupt:
            # Wait for last message to arrive
            await reader.get_message()
            global debug
            print("Done!")

            # Clean-up
            notifier.stop()
            mqtt_client1.disconnect()
            mqtt_client2.disconnect()
            pass  # exit normally


async def run_async_server(config):
    print("Register task for interval tasks")
    asyncio.create_task(interval_tasks(config))
    while True:
      print("will start a can listener....")
      await asyncio.sleep(1)
      await StartCAN(config)

if __name__ == '__main__':
    run_updating_server()

