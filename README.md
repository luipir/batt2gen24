Looking for Gen24/BYD modbus registers, scripts (passive modbus rtu listener, modbus tcp client, etc), wiring of Tesla M3 battery....head over to: 
https://gitlab.com/pelle8/gen24


# Batt2Gen24

So, now with the support for Sungrow (SH10RT tested) inverter the project should be called something else...It's more of faking a BYD battery, be it over modbus RTU or CAN towards the inverter.

The solution is based on different handlers with a brain in the middle, using json over mqtt to commuicate with each other. The BMS is connected to the RPi with a can-interface. A generic bms-handler with the help of a configurable specific bms-decoder gets can-data into json-formats, later pubilshed over mqtt. The brain(logic) is subscribing on bms-data and inverter-data (the registers that the inverter writes), making sure that everything is fine, and turns all required data into modbus register, in json and publishes this. The inverter will get these registers over mqtt and put them in local registers, to be read by the inverter. A stats module and a web-page shows an overview of the cells, soc, energy etc.

As for the logic when it comes the Tesla, not much has been done so far - it's hard without the hardware....basic functionality for sending commands over mqtt is in place, so if you wish to close the contactor you can publish that and that will trigger a certain can-message to be sent. And if you need an extra relay/contactor this can also be controlled over mqtt.


## Status
Current BMS support:
- Batrium: Charging and discharging ok.
- TeslaM3: Charging and discharging ok. Contactor closing implemented.
- Juntek (VAT4300, shunt): Charging and discharging ok.
- Leaf: Only tested with candump, decodes data ok.

Current Inverter support:
- Gen24: OK
- Sungrow: OK

## Known limitiations
- Sungrow: target values for current seem to be ignored when running in compulsory mode.
- Juntek:  Since Juntek shunt isn't truly a BMS, it will not do any balancing. SoC is based on voltage if Ah-counter differs too much from voltage-calculation.

## Batt2gen24, architecture
![Battery2Gen24](./batt2gen24.png "arch")


## Gen24, faking a BYD over modbus RTU
![Gen24](./images/gen24.png "Gen24")


## Sungrow, faking a BYD over CAN
![Gen24](./images/sungrow.png "Gen24")
Since the GUI is hmmm, not so useful, a sample script that can control power over modbus tcp is here.... (./sungrow_control.py)


## Batt2gen24 - stats, screenshot from my old Leaf pack with Batrium BMS.
![Leaf/Batrium](./images/batt_balancing.png "Batrium/Leaf")



## Batt2gen24 - stats, screenshot from a Tesla M3 pack.
![Tesla](./images/tesla_m3.png "Tesla")



## The modules

bms_handler:
- A module per type of bms (batrium, leaf, tesla...)
- Receives can messages from bms, decodes and publishes them in a generic format
- Subscribes on control data, if any, that needs to be sent to bms (close contactor etc)

ui_stats:
- subscribes and show statistics on web-page

ui_config:
- web/file: choice of bms/inverter
- web/file: extra parameters for logic
- publishes configuration

logic:
- subscribes on bms data, transforms this into modbus registers
- subscribes on configuration, transforms this into information that inverters require + control logic
- subscribes on inverter_registers, handles state transitions (startup)
- publishes modifications to modbus registers
- publishes can control, if any
- adds functionality in addition to just transforming messages, like limiting current if cell_voltage is low or disabling charging/discharging if outside limits (in addition to inverter limits)

inverter_handler:
- A module per type of inverter, only Gen24 and Sungrow SHxxRT for now. Gen24 talks modbus RTU, while Sungrow is on CAN.
- Keeps the register content in memory
- Receives modbus messages from inverter. "Reads" - just responds with the content of the local registers. "Writes" - writes to local registers.
- Publishes registers 
- Subscribes on inverter_register (from logic) and updates local registers
- Checks that the soc has been updated recently (trap errors in logic or bms_handler), if not put the "battery" in FAULT.

mqtt_broker:
- handles pub/sub


## JSON Data over MQTT

### From BMS
This is data from the bms that different modules will need.

Example - subscribing on bms/stat: `$ mosquitto_sub -h localhost -t bms/stat -u "batt2gen24" -P "batt2gen24_pass"`

The following will be published from bms_handler (not 100% updated):
- [bms/stat - soc, current power etc](./structures/bms_stat.json)
- [bms/cellstat - individual cell stats](./structures/bms_cellstat.json)
- [bms/static - number of cells, nominal capacity etc](./structures/bms_static.json)
- [bms/target - target current charge/discharge etc](./structures/bms_target.json)
- [bms/specific - bms specific, contactor state etc ](./structures/bms_specific.json)


### To BMS
This is commands to the bms (if needed). 
Control like "close_contactor"...on Tesla: `$ mosquitto_pub -h localhost -m "close_contactor" -t "bms/control" -u "batt2gen24" -P "batt2gen24_pass" ` 

bms_handler will subscribe to:
- bms/control   ["close_contactor",....]


### From inverter
Content of modus register from inverter to logic:
- [inverter/writes - 401,1001](./structures/inverter_writes.json)


### To inverter
Content of modus registers to inverter from logic:
- [inverter/data - ...last_updated_soc...](./structures/inverter_data.json)


### To external
This is commands to that can close relays/contactors connected to the pi.
Control like "close relay 1"...: `$ mosquitto_pub -h localhost -m "{"type":"relay","id":0,"state":"on"}" -t "external/control" -u "batt2gen24" -P "batt2gen24_pass" ` 
This is implemented in the logic module for Tesla, enabling precharge with Gen24 disconnected from the Tesla battery.

- [external/control](./structures/external_control.json)


## Todo/Status
general:
- code cleaning and reusing....as always
external_handler:
- Relay: implemented, not tested


config:
- No ui for now, only static config in files

# Installation and configuration


## MQTT broker

Install and configure, allow mqtt over websockets.

```
sudo apt install mosquitto mosquitto-clients
sudo systemctl enable mosquitto
sudo mosquitto_passwd -c /etc/mosquitto/.passwd batt2gen24   # type "batt2gen24_pass" when asked for password
sudo nano /etc/mosquitto/conf.d/auth.conf
...content:
listener 1883
allow_anonymous false
password_file /etc/mosquitto/.passwd

sudo nano /etc/mosquitto/conf.d/websockets.conf 
...content:
listener 9001
protocol websockets

sudo systemctl restart mosquitto
```



## batt2gen24

- install and configure mqtt broker (see above)
- create dir on pi: `mkdir /opt/batt2gen24`
- copy all .py and .yaml to dir above
- modify configuration files (yaml) to fit your setup
- make sure can-interface is up: `sudo ifconfig can0 txqueuelen 100`
- make sure can-interface is up: `sudo ip link set can0 type can bitrate 500000`
- repeat the two steps above if you also have a can1 
- copy www/stats.html to your web-server, edit the file and change the "mqtt_host" to pi IP address, make sure you user can write to the file (chmod/chown)
- either run all scripts manually or as systemd services
- Manual: bring up three windows (or tmux sessions) and start a python process in each session:
```
pi@pi(1):/opt/batt2gen24 $ python3 bms_handler.py
pi@pi(2):/opt/batt2gen24 $ python3 logic_handler.py
pi@pi(3):/opt/batt2gen24 $ python3 inverter_handler.py
pi@pi(4):/opt/batt2gen24 $ python3 stats_collector.py
pi@pi(5):/opt/batt2gen24 $ python3 external_handler.py
```
- Services:copy the files in systemd to pi:/etc/systemd/system
```
pi@pi:/opt/batt2gen24 $ sudo systemctl daemon-reload
pi@pi:/opt/batt2gen24 $ sudo systemctl enable batt2gen24_bms_handler
pi@pi:/opt/batt2gen24 $ sudo systemctl enable batt2gen24_inverter_handler
pi@pi:/opt/batt2gen24 $ sudo systemctl enable batt2gen24_logic_handler
pi@pi:/opt/batt2gen24 $ sudo systemctl enable batt2gen24_stats_collector
pi@pi:/opt/batt2gen24 $ sudo systemctl enable batt2gen24_external_handler
pi@pi:/opt/batt2gen24 $ sudo systemctl start batt2gen24_bms_handler
pi@pi:/opt/batt2gen24 $ sudo systemctl start batt2gen24_inverter_handler
pi@pi:/opt/batt2gen24 $ sudo systemctl start batt2gen24_logic_handler
pi@pi:/opt/batt2gen24 $ sudo systemctl start batt2gen24_stats_collector
pi@pi:/opt/batt2gen24 $ sudo systemctl start batt2gen24_external_handler

```
- Logs will end up in syslog, but normally a bit delayed. You can also change the service so it starts in a tmux to be able to get the logs closer to real time, check out batt2gen24_inverter_handler.service which uses tmux (to attach: `tmux a -t inverter_handler`)

## Virtual can interface

When playing around with the bms_handler, it's quite convenient to replay saved dumps instead of always being connected to real hardware. To get vcan up and running...
```
$ sudo modprobe vcan
$ sudo ip link add dev vcan0 type vcan
$ sudo ip link set up vcan0
$ canplayer -I m3-michael.log
```
