#!/usr/bin/env python
import os
# 2023 Per Carlén
#
# This is a handler for "external", which could be something like usb-relays
# Subscribes on control commands that will be sent to vms
#  test pub with: $ mosquitto_pub -h localhost -m "close_relay" -t "external/control" -u "batt2gen24" -P "batt2gen24_pass"
#
#

import time
import json
import yaml
import re
import paho.mqtt.client as paho
import asyncio
from serial import Serial

# global vars
external_debug = 0

def read_config_file():

    config_json = ""
    filename = "config.yaml"
    path = "/etc/batt2gen24/"
    if os.path.isfile(path+filename):
        filename = path + filename
    print("reading from:",filename)
    try:
      with open(filename,'r') as file:
        config_json = json.dumps(yaml.safe_load(file))
    except:
      print("ERROR: No config file - ",filename)
      exit(1)

    return config_json

def mqtt_on_publish(mqtt_client,userdata,result): 
    #print("data published:",result)
    pass

def mqtt_on_message(mqtt_client,userdata, message,tmp=None):
    global external_debug
    global external_relays

    if external_debug:
      pass
      #print(" Received message " + str(message.payload)
      #+ " on topic '" + message.topic
      #+ "' with QoS " + str(message.qos))
    if message.topic == "external/control":
      json_data = json.loads(message.payload)
      if 'type' in json_data:
        if json_data['type'] == 'relay':
          relay_id = json_data['id']
          state = json_data['state']
          for r in external_relays:
            if r['id'] == relay_id:
              if external_debug:
                print("Relay",r['device'],state)
              serial_port = Serial(r['device'], r['device_speed'])
              if state == "on":
                command = r['close_relay_command']
              if state == "off":
                command = r['open_relay_command']
              serial_port.write(bytearray(command.encode('utf-8')))


def mqtt_on_subscribe(client, userdata, mid, granted_qos):
    pass

def mqtt_on_connect(client, userdata, flags, rc):
    if rc != 0:
      print("MQTT connection problem")
      client.connected_flag=False
    else:
      print("MQTT client connected:" + str(client))
      client.connected_flag=True

def publish(client,pub_var,topic):
    json_string = json.dumps(pub_var)
    ret = client.publish(topic,json_string) 


async def main() -> None:
    global mqtt_client1
    global mqtt_client2
    global external_debug
    global external_relays

    print("\nStarting external_handler")
    print("Reading config from file")
    config = json.loads(read_config_file())
    external_relays = []
    if config:
      if 'name' in config['external_relays'][0]:
        external_relays = config['external_relays']
      external_debug = config['external_debug']
      mqtt_broker = config['mqtt_broker']
      mqtt_port = config['mqtt_port']
      mqtt_username = config['mqtt_username']
      mqtt_password = config['mqtt_password']
    else:
      exit(1)

    # this mqtt client will publish, what...state?
    mqtt_client1 = paho.Client(client_id="external_handler_pub",transport="tcp",protocol=paho.MQTTv311,clean_session=True)  
    mqtt_client1.on_publish = mqtt_on_publish
    mqtt_client1.on_connect = mqtt_on_connect
    mqtt_client1.username_pw_set(mqtt_username, mqtt_password)
    mqtt_client1.connect(mqtt_broker,mqtt_port,keepalive=60)
    mqtt_client1.loop_start()

    # this mqtt client will subscribe
    mqtt_client2 = paho.Client(client_id="external_handler_sub",transport="tcp",protocol=paho.MQTTv311,clean_session=True)  
    mqtt_client2.on_subscribe = mqtt_on_subscribe
    mqtt_client2.on_connect = mqtt_on_connect
    mqtt_client2.on_message = mqtt_on_message
    mqtt_client2.username_pw_set(mqtt_username, mqtt_password)
    mqtt_client2.connect(mqtt_broker,mqtt_port,keepalive=60)
    mqtt_client2.subscribe("external/control",2)
    mqtt_client2.loop_start()

    try:
        while True:
            await asyncio.sleep(2)

    except KeyboardInterrupt:
        # Wait for last message to arrive
        await reader.get_message()
        global debug
        print("Done!")

        # Clean-up
        notifier.stop()
        mqtt_client1.disconnect()
        mqtt_client2.disconnect()
        pass  # exit normally


if __name__ == "__main__":
    asyncio.run(main())

