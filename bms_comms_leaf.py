#!/usr/bin/env python

# 2023 Per Carlén
#
# This is a decoder for Leaf BMS.
# This module decodes a can-message, and returns a json-string

import can
import json
import struct
import re
import time

battery_request_idx = 0
main_cell_voltages = {}
ignore_7bb = True
group_7bb = 0
can_storage = {}


def leaf_decode(msg: can.Message,bms_debug) -> None:
    global main_cell_voltages
    global battery_request_idx
    global ignore_7bb
    global group_7bb
    global can_storage

    time_now = time.time()
    id = hex(msg.arbitration_id)
    data = msg.data

    if id == "0x55b":
      d1 = data[0] * 4
      d2 = (data[1] & 192) >> 6
      soc = int(d1 + d2) / 10
      #print("soc:",soc,data)
      json_string = json.dumps({'last_update' : time_now, 'stat_soc': soc }, separators=(',', ':'))
      return json_string

    if id == "0x5c0": # Always reads temp as 0 degrees, better read them instead as with cell voltages
      mux = data[0] >> 6
      temp = (data[2] << 1) * 2 - 40 
      #print("mux",mux,temp)
      return False

    if id == "0x1dc": # Target charge/discharge? (this is limit values)
      d1 = data[0] << 2 
      d2 = (data[1] & 192) >> 6
      target_discharge_power = int(d1 + d2) * 4 
      d1 = (data[1] & 63) << 4 
      d2 = (data[2] & 240) >> 4
      target_charge_power = int(d1 + d2) * 4 
      #print("C,D:",target_charge_power,target_discharge_power)
      return False

    if id == "0x1db":
      d1 = data[0] * 4
      d2 = (data[1] & 224) >> 5
      batt_amps = int(d1 + d2) / 2
      if batt_amps > 1024:
       batt_amps = - (2048 - batt_amps)
      d1 = data[2] * 4
      d2 = (data[3] & 192) >> 6
      batt_voltage = int(d1 + d2) / 2
      #print("U/I:",batt_voltage,batt_amps,data)
      batt_power = batt_voltage * batt_amps
      json_string = json.dumps({'last_update' : time_now, 'stat_batt_voltage': batt_voltage, 'stat_batt_current': batt_amps, 'stat_batt_power': batt_power }, separators=(',', ':'))
      return json_string

    if id == "0x7bb": # Here comes a lot of stuff...
      if data[0] == 16:
        ignore_7bb = True
        group_7bb = data[3]
        if group_7bb == 1 or group_7bb == 2 or group_7bb == 4 or group_7bb == 6: ignore_7bb = False
      if not ignore_7bb:

        if group_7bb == 1: # soc, current, voltage etc - much more accurate than 0x1db - TODO: replace
          # send next_line_request
          if data[0] == 16:
            can_storage = {}
            can_storage.update( {data[0]:data[0:8] })
            return False

          if data[0] >32 and data[0] < 40:
            can_storage.update( {data[0]:data[0:8] })

          if data[0] == 0x27 and data[6] ==0xFF : # last frame
            #print("cs:",can_storage)
            h = (( can_storage[36][4] << 8) | can_storage[36][5]) / 102.4
            soc = ( can_storage[36][7] << 16 | (can_storage[37][1] << 8) | can_storage[37][2] ) /10000
            ahr = ( can_storage[37][4] << 16 | (( can_storage[37][5] << 8) | can_storage[37][6]))/10000
            batt_i = ( can_storage[16][4] << 24) | ( can_storage[16][5] << 16 | (( can_storage[16][6] << 8) | can_storage[16][7]))
            batt_u = (( can_storage[35][1] << 8) | can_storage[35][2]) / 100
            if batt_i & 0x8000000 == 0x8000000 :
              batt_i = (batt_i | -0x100000000 ) / 1024
            else:
              batt_i = batt_i / 1024
            print("soc:",soc,h,ahr,batt_i,batt_u)
            return False


        if group_7bb == 2: # Cell voltages
          # send battery_next_line_request
          if data[0] == 16:
            main_cell_voltages = [0] * 100
            battery_request_idx = 0
            main_cell_voltages[battery_request_idx] = data[4] << 8 | data[5]
            battery_request_idx += 1
            main_cell_voltages[battery_request_idx] = data[6] << 8 | data[7]
            battery_request_idx += 1
            return False

          if data[0] == 0x2C and data[6] ==0xFF : # last frame
            json_temp = ""
            cell_min = 5
            cell_max = 0
            for cell_no in range(0,96):
              cell_voltage = main_cell_voltages[cell_no] / 1000
              if cell_voltage > 0 and cell_voltage < 5:
                if cell_voltage > cell_max: cell_max = cell_voltage
                if cell_voltage < cell_min: cell_min = cell_voltage
                json_temp += json.dumps({'last_update' : time_now, 'cellstat_cell_num': cell_no + 1 , 'cellstat_max_voltage': cell_voltage, 'cellstat_min_voltage': cell_voltage }) + ","
            json_stat = json.dumps({'last_update' : time_now, 'stat_min_cell_u': cell_min, 'stat_max_cell_u': cell_max}, separators=(',', ':'))
            json_string = "[" + json_temp + json_stat + "]"
            #print("js:",json_string)
            return json_string

          #print("data:",data)
          if data[0] % 2 == 0: # even frames
            main_cell_voltages[battery_request_idx] |= data[1]
            battery_request_idx += 1
            main_cell_voltages[battery_request_idx] = data[2] << 8 | data[3]
            battery_request_idx += 1
            main_cell_voltages[battery_request_idx] = data[4] << 8 | data[5]
            battery_request_idx += 1
            main_cell_voltages[battery_request_idx] = data[6] << 8 | data[7]
            battery_request_idx += 1
          else:  
            main_cell_voltages[battery_request_idx] = data[1] << 8 | data[2]
            battery_request_idx += 1
            main_cell_voltages[battery_request_idx] = data[3] << 8 | data[4]
            battery_request_idx += 1
            main_cell_voltages[battery_request_idx] = data[5] << 8 | data[6]
            battery_request_idx += 1
            main_cell_voltages[battery_request_idx] = data[7] << 8

        if group_7bb == 4: # temperature 
          # send next_line_request
          if data[0] == 16:
            can_storage = {}
            can_storage.update( {data[0]:data[0:8] })
            return False

          if data[0] >32 and data[0] < 35:
            can_storage.update( {data[0]:data[0:8] })

          if data[0] == 0x22 and data[6] ==0xFF : # last frame
            #print("cs:",can_storage)
            temp = [0] * 4
            min_temp = 99
            max_temp = -99
            temp[0] = -40 + int((can_storage[16][4]) << 8 | (can_storage[16][5])) / 10
            temp[1] = -40 + int((can_storage[16][7] << 8 | can_storage[33][1])) / 10
            temp[2] = -40 + int((can_storage[33][3] << 8 | can_storage[33][4])) / 10
            temp[3] = -40 + int((can_storage[33][6] << 8 | can_storage[33][7])) / 10        
            for t in temp:
              if t > -50 and t < 100:
                if t > max_temp: max_temp = t
                if t < min_temp: min_temp = t
            #print("temp:",min_temp,max_temp)
            json_temp1 = json.dumps( {'last_update' : time_now, 'cellstat_cell_num': (1), 'cellstat_cell_temp': min_temp } )
            json_temp2 = json.dumps( {'last_update' : time_now, 'cellstat_cell_num': (2), 'cellstat_cell_temp': max_temp } )
            json_string = "[" + json_temp1 + "," + json_temp2 + "]"
            return json_string

        if group_7bb == 6: # balancing - shunts 
          # send next_line_request
          if data[0] == 16:
            can_storage = {}
            can_storage.update( {data[0]:data[0:8] })
            return False

          if data[0] >32 and data[0] < 36:
            can_storage.update( {data[0]:data[0:8] })

          if data[0] == 0x23 and data[6] ==0xFF : # last frame
            shunt_data = can_storage[16][4:8] + can_storage[33][0:8] + can_storage[34][0:1]
            shunt_status = [0] * 96
            shunt_bits = [-1.0] * 24
            shunt_order = "8421"
            for bit in range(12):
              shunt_bits[bit*2] = shunt_data[bit] & 0xF
              shunt_bits[(bit*2) + 1] = shunt_data[bit] >> 4
            json_temp = ""
            for i in range(96):
              i3 = shunt_bits[i >> 2]
              j = int(shunt_order[i % 4])
              if (j & i3) != 0 :
                shunt_status[i] = 1
              bypass_value = shunt_status[i] * 50
              #print("shunt:",i,bypass_value)
              json_temp += json.dumps({'last_update' : time_now, 'cellstat_cell_num': i + 1, 'cellstat_cell_balancing': bypass_value}) + ","
            new_json = re.sub(r",$", "", json_temp)
            json_string = "[" + new_json + "]"
            return json_string

    return False
