#!/usr/bin/env python

# 2023 Per Carlen
# Parts from: Tesla Model 3/Y Python Connector by Instagram: alex4skate\nThanks to bielec, Bryan Inkster, Jack Rickard and Collin Kidder"
#
# This is a decoder for Tesla Model3 BMS.
# This module decodes a can-message, and returns a json-string

import can
import json
import struct
import re
import time


def bytes_to_int_reverse(msg, pos, byte_len):
    result = 0
    for index in range(pos + byte_len - 1, pos - 1, -1):
        result = result * 256 + int(msg.data[index])
    return result


def extract_k_bits(bytes_value, position, bit_quantity, little):
    # convert number into binary first
    binary = ""
    position = position
    if little:
        for index in range(0, len(bytes_value)):
            binary_dummy = bin(bytes_value[len(bytes_value) - index - 1])
            # remove first two characters 0b that bin add in the front
            binary += fill_up_bits_string(binary_dummy[2:])
        end = len(binary) - position
        start = len(binary) - position - bit_quantity
    else:
        for index in range(0, len(bytes_value)):
            binary_dummy = bin(bytes_value[index])
            # remove first two characters 0b that bin add in the front
            binary += fill_up_bits_string(binary_dummy[2:])
        end = position + bit_quantity
        start = position

    k_bit_sub_str = binary[start: end]
    return int(k_bit_sub_str, 2)

def fill_up_bits_string(bits):
    result_bits = ""
    for index in range(len(bits), 8):
        result_bits += "0"
    return result_bits + bits



def decode(msg: can.Message,bms_debug) -> None:
    time_now = time.time()
    id = hex(msg.arbitration_id)
    length = len(msg.data)
    #print("Tesla Id:",id,length)

    if id == "0x332": # min max volt,temp, seems there are no messages for individual cell temp
        mux = (msg.data[0])
        mux = mux & 0x03
        if mux == 1:  # then pick out max / min cell volts
            volts = bytes_to_int_reverse(msg, 0, 2)
            volts >>= 2
            volts = volts & 0xFFF
            max_volts = volts / 500

            volts = bytes_to_int_reverse(msg, 2, 2)
            volts = volts & 0xFFF
            min_volts = volts / 500.0

            volts = (msg.data[4])
            max_vno = 1 + (volts & 0x007F)

            volts = (msg.data[5])
            min_vno = 1 + (volts & 0x007F)
            #print("Cell No: ", max_vno, "= max: ", max_volts, "V Cell No:", min_vno, "= min:", min_volts, "V")
            json_string = json.dumps({'last_update' : time_now, 'stat_min_cell_u': min_volts, 'stat_max_cell_u': max_volts}, separators=(',', ':'))
            return json_string
        if mux == 0:  # then pick out max / min temperatures
            volts = (msg.data[2])
            max_temp = int((volts * 0.5) - 40)
            volts = (msg.data[3])
            min_temp = int((volts * 0.5) - 40)
            json_temp1 = json.dumps( {'last_update' : time_now, 'cellstat_cell_num': (1), 'cellstat_cell_temp': min_temp ,'stat_min_cell_t': min_temp, 'stat_max_cell_t': max_temp } )
            json_temp2 = json.dumps( {'last_update' : time_now, 'cellstat_cell_num': (2), 'cellstat_cell_temp': max_temp } )
            json_string = "[" + json_temp1 + "," + json_temp2 + "]"
            return json_string

    if id == "0x401": # cell stats
      mux = bytes_to_int_reverse(msg, 0, 1)  # get mux
      cell = [0] * 255
      volts = msg.data[1]  # status byte must be 0x02A
      if volts == 0x02A:
          volts = bytes_to_int_reverse(msg, 2, 2)
          volts = volts / 10000.0
          cell[1 + mux * 3] = volts
          volts = bytes_to_int_reverse(msg, 4, 2)
          volts = volts / 10000.0
          cell[2 + mux * 3] = volts
          volts = bytes_to_int_reverse(msg, 6, 2)
          volts = volts / 10000.0
          cell[3 + mux * 3] = volts
          json_temp1 = json.dumps( {'last_update' : time_now, 'cellstat_cell_num': (1 + mux * 3), 'cellstat_max_voltage': cell[1 + mux * 3], 'cellstat_min_voltage': cell[1 + mux * 3] } )
          json_temp2 = json.dumps( {'last_update' : time_now, 'cellstat_cell_num': (2 + mux * 3), 'cellstat_max_voltage': cell[2 + mux * 3], 'cellstat_min_voltage': cell[2 + mux * 3] } )
          json_temp3 = json.dumps( {'last_update' : time_now, 'cellstat_cell_num': (3 + mux * 3), 'cellstat_max_voltage': cell[3 + mux * 3], 'cellstat_min_voltage': cell[3 + mux * 3] } )
          json_string = "[" + json_temp1 + "," + json_temp2 + "," + json_temp3 + "]"
          return json_string

    if id == "0x352": # soc etc
      energy_buffer = extract_k_bits(msg.data, 55, 8, True) * 0.1
      energy_to_charge_complete = extract_k_bits(msg.data, 44, 11, True) * 0.1
      expected_energy_remaining = extract_k_bits(msg.data, 22, 11, True) * 0.1
      full_charge_complete = extract_k_bits(msg.data, 63, 1, True)
      ideal_energy_remaining = extract_k_bits(msg.data, 33, 11, True) * 0.1
      nominal_energy_remaining = extract_k_bits(msg.data, 11, 11, True) * 0.1
      nominal_full_pack_energy = extract_k_bits(msg.data, 0, 11, True) * 0.1
      json_string = json.dumps({'last_update' : time_now, 'stat_nominal_full_pack_energy': nominal_full_pack_energy }, separators=(',', ':'))
      return json_string

    if id == "0x292":  # soc_vi as soc
        bat_beginning_of_life = extract_k_bits(msg.data, 40, 10, True) / 10
        soc_max = extract_k_bits(msg.data, 20, 10, True) / 10
        soc_ave = extract_k_bits(msg.data, 30, 10, True) / 10
        soc_vi = extract_k_bits(msg.data, 10, 10, True) / 10
        soc_min = extract_k_bits(msg.data, 0, 10, True) / 10
        bms_bat_temp_pct = extract_k_bits(msg.data, 50, 8, True) * 0.4
        json_string = json.dumps({'last_update': time_now, 'stat_soc': soc_vi, 'stat_bat_beginning_of_life': bat_beginning_of_life}, separators=(',', ':'))
        return json_string

    if id == "0x132": # volts etc
      volts = extract_k_bits(msg.data, 0, 16, True) * 0.01
      amps = extract_k_bits(msg.data, 16, 16, True) # * 0.1
      if amps > 32768:
        amps = - (65535 - amps)
      amps = amps * 0.1
      raw_amps = extract_k_bits(msg.data, 32, 16, True) * -0.05
      minutes = extract_k_bits(msg.data, 48, 12, True) * 0.1
      power = volts * amps
      json_string = json.dumps({'last_update' : time_now, 'stat_batt_voltage': volts, 'stat_batt_current': amps, 'stat_batt_power': power }, separators=(',', ':'))
      return json_string

    if id == "0x2d2": # min, max voltage, current is probably not what we want
        min_voltage = extract_k_bits(msg.data, 0, 16, True) * 0.01 * 2
        #max_discharge_current = extract_k_bits(msg.data, 48, 14, True) * 0.128
        #max_charge_current = extract_k_bits(msg.data, 32, 14, True) * 0.1
        max_voltage = extract_k_bits(msg.data, 16, 16, True) * 0.01 * 2
        json_string = json.dumps({'last_update' : time_now, 'static_max_voltage': max_voltage, 'static_min_voltage': min_voltage}, separators=(',', ':'))
        return json_string

    if id == "0x20a": # contactor state etc
        contactor = int(msg.data[1] & 0x0F)
        hvil_status = extract_k_bits(msg.data, 40, 4, True)
        packContNegativeState = extract_k_bits(msg.data, 0, 3, True)
        packContPositiveState = extract_k_bits(msg.data, 3, 3, True)
        packContactorSetState = extract_k_bits(msg.data, 8, 4, True)
        packCtrsClosingAllowed = extract_k_bits(msg.data, 35, 1, True)
        pyroTestInProgress = extract_k_bits(msg.data, 37, 1, True)

        contactorText = ["UNKNOWN(0)",
                        "OPEN    ",
                        "CLOSING ",
                        "BLOCKED ",
                        "OPENING ",
                        "CLOSED  ",
                        "UNKNOWN6",
                        "WELDED  ",
                        "POS CL  ",
                        "NEG CL  ",
                        "UNKNOWN10",
                        "UNKNOWN11",
                        "UNKNOWN12",
                        "UNKNOWN13",
                        "UNKNOWN14",
                        "UNKNOWN15"]
        contactorState = ["SNA       ",
                        "OPEN      ",
                        "PRECHARGE ",
                        "BLOCKED   ",
                        "PULLED_IN ",
                        "OPENING   ",
                        "ECONOMIZED",
                        "WELDED    ",
                        "UNKOWN(8)",
                        "UNKOWN(9)",
                        "UNKOWN(10)",
                        "UNKOWN(11)",
                        "UNKOWN(13)",
                        "UNKOWN(14)",
                        "UNKOWN(15)",
                        "UNKOWN(16)"
                          ]
        hvilStatusState=["UNKNOWN(0)",
                        "STATUS_OK",
                        "CURRENT_SOURCE_FAULT",
                        "INTERNAL_OPEN_FAULT",
                        "VEHICLE_OPEN_FAULT",
                        "PENTHOUSE_LID_OPEN_FAULT",
                        "UNKNOWN_LOCATION_OPEN_FAULT",
                        "VEHICLE_NODE_FAULT",
                        "NO_12V_SUPPLY",
                        "VEHICLE_OR_PENTHOUSE_LID_OPENFAULT",
                        "UNKNOWN(10)",
                        "UNKNOWN(11)",
                        "UNKNOWN(12)",
                        "UNKNOWN(13)",
                        "UNKNOWN(14)",
                        "UNKNOWN(15)"]
        try:
            json_string = json.dumps({'last_update' : time_now, 'specific_contactor': contactorText[contactor], 'specific_hvil_status': hvilStatusState[hvil_status],
                'specific_packContNegativeState': contactorState[packContNegativeState], 'specific_packContPositiveState': contactorState[packContPositiveState],
                'specific_packContactorSetState': contactorState[packContactorSetState], 'specific_packCtrsClosingAllowed': packCtrsClosingAllowed,
                'specific_pyroTestInProgress': pyroTestInProgress}, separators=(',', ':'))
        except:
            json_string = json.dumps({'last_update' : time_now, 'specific_contactor': contactor, 'specific_hvil_status': hvil_status,
                'specific_packContNegativeState': packContNegativeState, 'specific_packContPositiveState': packContPositiveState,
                'specific_packContactorSetState': packContactorSetState, 'specific_packCtrsClosingAllowed': packCtrsClosingAllowed,
                'specific_pyroTestInProgress': pyroTestInProgress}, separators=(',', ':'))

        return json_string




    return False

def send_command(bus,msg,bms_debug) -> None:
    time_now = time.time()
    command = str(msg)
    if command == "close_contactor": # stay closed without sending periodically
      if bms_debug: print("Time to send:" + command + " to tesla bms")
      msg = can.Message(arbitration_id=0x221, timestamp=time.time(),
                        data=[0x40, 0x41, 0x05, 0x15, 0x00, 0x50, 0x71, 0x7f], is_extended_id=False)

      try:
          bus.send(msg)
          if bms_debug: print("Contactor Frame 221 61 was successful sent.")
      except can.CanError:
          print(msg, "Contactor Frame 221 61 can not be sent.")
      msg = can.Message(arbitration_id=0x221, timestamp=time.time(),
                        data=[0x60, 0x55, 0x55, 0x15, 0x54, 0x51, 0xd1, 0xb8], is_extended_id=False)
      try:
          bus.send(msg)
          if bms_debug: print("Contactor Frame 221 60 was successful sent.")
      except can.CanError:
          print(msg, "Contactor Frame 221 60 can not be sent.")
      return True

    if command == "close_contactor_interval": # test

# From bielec: My fist 221 message, to close the contactors is 0x41, 0x11, 0x01, 0x00, 0x00, 0x00, 0x20, 0x96 and then, 
# to cause "hv_up_for_drive" I send an additional 221 message 0x61, 0x15, 0x01, 0x00, 0x00, 0x00, 0x20, 0xBA  so 
# two 221 messages are being continuously transmitted.   When I want to shut down, I stop the second message and only send 
# the first, for a few cycles, then stop all  messages which causes the contactor to open.

      if bms_debug: print("Time to send:" + command + " to tesla bms")
      msg = can.Message(arbitration_id=0x221, timestamp=time.time(),
                        data=[0x41, 0x11, 0x01, 0x00, 0x00, 0x00, 0x20, 0x96], is_extended_id=False)
      try:
          bus.send(msg)
          if bms_debug: print("Contactor Frame 221 - hv_up_for_drive")
      except can.CanError:
          print(msg, "Contactor Frame 221 61 can not be sent.")

      msg = can.Message(arbitration_id=0x221, timestamp=time.time(),
                        data=[0x61, 0x15, 0x01, 0x00, 0x00, 0x00, 0x20, 0xba], is_extended_id=False)
      try:
          bus.send(msg)
          if bms_debug: print("Contactor Frame 221 - hv_up_for_drive")
      except can.CanError:
          print(msg, "Contactor Frame 221 61 can not be sent.")


      return True



    if command == "open_contactor": # this doesn't work yet...don't know what to send

      msg = can.Message(arbitration_id=0x332, data=[0x61, 0x15, 0x01, 0x55, 0x00, 0x00, 0xe0, 0x13], is_extended_id=False)
      try:
        bus.send(msg)
      except can.CanError:
          print(msg, "Contactor Frame 332 can not be sent.")

#      msg = can.Message(arbitration_id=0x332, data=[0x3C, 0x0C, 0x85, 0x80, 0x89, 0x86], is_extended_id=False)
#      try:
#        bus.send(msg)
#      except can.CanError:
#          print(msg, "Contactor Frame 332 60 can not be sent.")

      return True

    print("No such command:" + command[0])
    return False
