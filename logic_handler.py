#!/usr/bin/env python

# 2023 Per Carlen
#
# This is the logic between a bms and an inverter
#
# Publishes register-changes to inverter-handler
#  test sub with: $ mosquitto_sub -h localhost -t bms/stat -u "batt2gen24" -P "batt2gen24_pass"
# Subscribes on bms data and register-changes from inverter
#
#

import hashlib
import os
import time
import copy
import asyncio
import json
import yaml
import re
import paho.mqtt.client as paho
from typing import List,Dict
import importlib


#from can.notifier import MessageRecipient

# Logic 

class LogicHandler:
  def __init__(self):
    self.config = self.read_config_file('config.yaml')

  @staticmethod
  def read_config_file(filename: str) -> Dict:
    config = ""
    filename = "config.yaml"
    path = "/etc/batt2gen24/"
    if os.path.isfile(path+filename):
      filename = path + filename
    print("Reading from:",filename)
    try:
      with open(filename,'r') as file:
        config = yaml.safe_load(file)
    except:
      print("ERROR: No config file - ",filename)
      exit(1)
    return config

# global vars
bms_type = ""
bms_update_interval = 60
inverter_type = ""

inverter_data = {}

bms_static_dict = {}
bms_stat_dict = {}
bms_cellstat_list = {}
bms_target_dict = {}
bms_specific_dict = {}
logic_config ={'logic_debug' : 0 }


def init_inverter_data(inverter,inverter_type):
  global inverter_data
  global bms_static_dict

  inverter_data_string = inverter.init_inverter_data(bms_static_dict) 
  if inverter_data_string:
    inverter_data = json.loads(inverter_data_string)
    #print("inverter:",inverter_data)
    #exit(1)


def read_config_file():

    config_json = ""
    filename = "config.yaml"
    path = "/etc/batt2gen24/"
    if os.path.isfile(path+filename):
        filename = path + filename
    print("reading from:",filename)
    try:
      with open(filename,'r') as file:
        config_json = json.dumps(yaml.safe_load(file))
    except:
      print("ERROR: No config file - ",filename)
      exit(1)

    return config_json


def interval_action(mqtt_client):
    global logic_config
    if bms_type == "tesla_model3": # Check that we are allowed to close contactors
              return
#      if 'specific_packCtrsClosingAllowed' in bms_specific_dict:
#        if 'specific_contactor' in bms_specific_dict:
#          if bms_specific_dict['specific_packCtrsClosingAllowed'] == 1:
#            if not re.search("CLOSED",bms_specific_dict['specific_contactor']):
              json_string = "close_contactor_interval"
              topic = "bms/control"
              ret = mqtt_client.publish(topic,json_string) 

def mqtt_on_publish(mqtt_client,userdata,result): 
    global logic_config
    pass

#def extract_minmax_temp_from_cellstats(bms_cellstat_list):
#    temp = bms_cellstat_list
#    temp_min = 99
#    temp_max = -99
#    for json_dict in temp:
#      if 'cellstat_cell_temp' in json_dict:
#        cell_temp = json_dict['cellstat_cell_temp']
#        if cell_temp > temp_max:
#          temp_max = cell_temp
#        if cell_temp < temp_min:
#          temp_min = cell_temp
#    if temp_min <99 and temp_max > -99:
#      ret_dict = { 'logic_cell_temp_min': temp_min, 'logic_cell_temp_max': temp_max }
#      return ret_dict
#    else:
#      return False

def mqtt_on_message(mqtt_client,userdata, message,tmp=None):
    #print(" Received message " + str(message.payload)
    #    + " on topic '" + message.topic
    #    + "' with QoS " + str(message.qos))
    global bms_specific_dict
    global bms_static_dict
    global bms_stat_dict
    global bms_cellstat_list
    global bms_target_dict  
    global inverter_data
    global bms_type
    global logic_config
    global have_all_static_vars
    global inverter

    json_data = json.loads(message.payload)
    if message.topic == "bms/static":
      bms_static_dict = json_data
    if message.topic == "bms/stat":
      bms_stat_dict = json_data
    if message.topic == "bms/cellstat":
      bms_cellstat_list = json_data
    if message.topic == "bms/target":
      bms_target_dict = json_data
    if message.topic == "bms/specific":
      bms_specific_dict = json_data

    bms_data = {}
    bms_data.update(bms_static_dict)
    bms_data.update(bms_stat_dict)
    bms_data.update(bms_target_dict)
    bms_data.update(bms_specific_dict)

    #rc = extract_minmax_temp_from_cellstats(bms_cellstat_list)
    #if type(rc) is dict:
    #  bms_data.update(rc)

    old_inverter_data = copy.deepcopy(inverter_data)
    # we will get registers back that we will publish
    temp = inverter.got_message(message.topic,message.payload,bms_data,inverter_data,logic_config,bms_type,mqtt_client)
#      if type(temp) is dict or type(temp) is list:
    if type(temp) is dict:
      inverter_data = temp
      old_json_str = json.dumps(old_inverter_data)
      new_json_str = json.dumps(inverter_data)
      new_json = re.sub(r"\"last_updated.*,", "", new_json_str)
      old_json = re.sub(r"\"last_updated.*,", "", old_json_str)
      sha_1_new = hashlib.sha1(new_json.encode('ascii')).hexdigest()
      sha_1_old = hashlib.sha1(old_json.encode('ascii')).hexdigest()
      if sha_1_new != sha_1_old:
        if logic_config:  print("new data to inverter...")
        publish_inverter_data(mqtt_client)


def mqtt_on_subscribe(client, userdata, mid, granted_qos):
    pass


def mqtt_on_connect(client, userdata, flags, rc):
    if rc != 0:
      print("MQTT connection problem")
      client.connected_flag=False
    else:
      print("MQTT client connected:" + str(client))
      client.connected_flag=True

def publish(client,pub_var,topic):
    json_string = json.dumps(pub_var)
    ret = client.publish(topic,json_string) 

def publish_inverter_data(mqtt_client):
    global inverter_data
    global logic_config
    global have_all_static_vars

    if have_all_static_vars:
      if logic_config['logic_debug']: print("Publishing inverter/data")
      if logic_config['logic_debug']: print(inverter_data)
#      json_string = json.dumps( inverter_data )
#      print(json_string)
      publish(mqtt_client,inverter_data,"inverter/data")


async def main() -> None:
    global mqtt_client1
    global mqtt_client2

    global bms_type
    global inverter_type
    global inverter_data

    global logic_config
    global have_all_static_vars
    global inverter

    logic_handler = LogicHandler()
    logic_config = logic_handler.config
    bms_type = logic_config['bms_type']
    inverter_update_interval = logic_config['inverter_update_interval']
    inverter_type = logic_config['inverter_type']
    mqtt_broker = logic_config['mqtt_broker']
    mqtt_port = logic_config['mqtt_port']
    mqtt_username = logic_config['mqtt_username']
    mqtt_password = logic_config['mqtt_password']

    print("\nStarting logic for " + inverter_type + " and " + bms_type)

    print("Loading module for inverter: ",logic_config['inverter_type'])
    #inverter = importlib.import_module('logic_' + logic_config['inverter_type'], package=None)
    inverter = importlib.import_module('logic', package=None)
    #inverter.run_updating_server(self)

    # this mqtt client will publish
    mqtt_client1 = paho.Client(client_id="logic_pub",transport="tcp",protocol=paho.MQTTv311,clean_session=True)  
    mqtt_client1.on_publish = mqtt_on_publish
    mqtt_client1.on_connect = mqtt_on_connect
    mqtt_client1.username_pw_set(mqtt_username, mqtt_password)
    mqtt_client1.connect(mqtt_broker,mqtt_port,keepalive=60)
    mqtt_client1.loop_start()

    # this mqtt client will subscribe
    mqtt_client2 = paho.Client(client_id="logic_sub",transport="tcp",protocol=paho.MQTTv311,clean_session=True)  
    mqtt_client2.on_subscribe = mqtt_on_subscribe
    mqtt_client2.on_connect = mqtt_on_connect
    mqtt_client2.on_message = mqtt_on_message
    mqtt_client2.username_pw_set(mqtt_username, mqtt_password)
    mqtt_client2.connect(mqtt_broker,mqtt_port,keepalive=60)
    mqtt_client2.subscribe("bms/stat",2)
    mqtt_client2.subscribe("bms/cellstat",2)
    mqtt_client2.subscribe("bms/static",2)
    mqtt_client2.subscribe("bms/target",2)
    mqtt_client2.subscribe("bms/specific",2)
    mqtt_client2.subscribe("inverter/writes",2)
    mqtt_client2.loop_start()

    #init_inverter_data(inverter,inverter_type)
    have_all_static_vars = False

    while not have_all_static_vars:
      must_have_keys = ['static_nominal_capacity','static_max_voltage','static_min_voltage','static_max_power']
      if all(k in bms_static_dict for k in must_have_keys):
        init_inverter_data(inverter,inverter_type)
        have_all_static_vars = True
      print("waiting for static bms-data")
      await asyncio.sleep(1)

    seconds = 0
    milliseconds = 0 # counts 10ms
    try:
        while True:
            if seconds > inverter_update_interval:
              publish_inverter_data(mqtt_client1)
              seconds = 0
            await asyncio.sleep(0.001)
            milliseconds += 1
            seconds += 0.001
            if milliseconds == logic_config['logic_interval']:
              interval_action(mqtt_client1)
              milliseconds = 0

    except KeyboardInterrupt:
        # Wait for last message to arrive
        await reader.get_message()
        print("Done!")

        # Clean-up
        notifier.stop()
        mqtt_client1.disconnect()
        mqtt_client2.disconnect()
        pass  # exit normally


if __name__ == "__main__":
    asyncio.run(main())

