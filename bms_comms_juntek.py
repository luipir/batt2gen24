#!/usr/bin/env python

# 2023 Per Carlen
#
# This is a decoder for Juntek BMS
#
# Inspiration from: https://github.com/opless/juntek-vat-python
#


import json
import struct
import re
import time

def decode(ba,bms_debug,batt_amp_hours_full,bms_specific_config) -> None:
    if bms_debug: 
      print("Arrived on modbus:",ba)
      for x in ba:
        print('0x{:02x} '.format(x),end='')
      print("")
    # Let's compute crc (last byte)...
    crc = 0
    for idx, b in enumerate(ba):
      if idx < len(ba) - 1: crc += b
    crc = crc & 255
    json_string = ""
    if crc == ba[len(ba)-1]:
      time_now = time.time()
      voltage = (ba[4] * 256 + ba[5]) / 100
      current = (ba[6] * 256 + ba[7]) 
      if current > 32767:
        current = - (current ^ 65535) + 1
      current = current / 10
      power = (ba[8] * 256 *256 *256  + ba[9]* 256 *256 +  ba[10]* 256 + ba[11]) / 1000
      amp_hours = (ba[12] * 256 *256 *256  + ba[13]* 256 *256 +  ba[14]* 256 + ba[15]) / 1000
      temp = ba[25] / 10
      if temp > 127:
        temp = - (temp ^ 65535) + 1
      if current < 0 and power > 0:
        power = -power
      batt_soc = int(amp_hours / batt_amp_hours_full)
      batt_soc_voltage_based = 100 * int(voltage - bms_specific_config['static_min_cell_voltage'])/ int(bms_specific_config['static_max_cell_voltage'] - bms_specific_config['static_min_cell_voltage'])
      if amp_hours == 0 or abs(batt_soc-batt_soc_voltage_based) > 20:
        batt_soc = batt_soc_voltage_based
        print("amp_hours are off(now:",amp_hours,"Ah, full:",batt_amp_hours_full,"Ah), using voltagebased soc instead")
      json_string = json.dumps({'last_update' : time_now, 'stat_soc': batt_soc, 'stat_batt_voltage': voltage, 'stat_batt_current': current, 'stat_batt_power': power , 'stat_min_cell_u': voltage, 'stat_max_cell_u': voltage, 'stat_min_cell_t': temp, 'stat_max_cell_t': temp }, separators=(',', ':'))
      if bms_debug: print("Decoded as:",json_string)
    else:
      print("ERROR: crc not ok")
    return json_string
