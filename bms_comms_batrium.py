#!/usr/bin/env python

# 2023 Per Carlen
#
# This is a decoder for Batrium BMS, using native protocol v1,
# This module decodes a can-message, and returns a json-string

import can
import json
import struct
import re
import time

def decode(msg: can.Message,bms_debug) -> None:
    time_now = time.time()
    id = hex(msg.arbitration_id)
    data = msg.data
    #print("Batrium Id:",id)
    if id == "0x111100":
      min_cell_u = (data[1] * 256 + data[0]) /1000
      max_cell_u = (data[3] * 256 + data[2]) /1000
      json_string = json.dumps({'last_update' : time_now, 'stat_min_cell_u': min_cell_u, 'stat_max_cell_u': max_cell_u}, separators=(',', ':'))
      return json_string
    if id == "0x111200":
      min_cell_t = data[0] - 40
      max_cell_t = data[1] - 40
      json_string = json.dumps({'last_update' : time_now, 'stat_min_cell_t': min_cell_t, 'stat_max_cell_t': max_cell_t}, separators=(',', ':'))
      return json_string
    if id == "0x111500":
      soc = (data[0] - 10) * 0.5
      batt_voltage = (data[3] * 256 + data[2]) /100
      batt_current = round( (struct.unpack("<f", data[4:8])[0] / 1000) , 3)  # Limit to three decimals
      batt_power = batt_voltage * batt_current
      json_string = json.dumps({'last_update' : time_now, 'stat_soc': soc, 'stat_batt_voltage': batt_voltage, 'stat_batt_current': batt_current, 'stat_batt_power': batt_power }, separators=(',', ':'))
      return json_string
    if id == "0x140300":
      max_voltage = (data[7] * 256 + data[6]) /10
      charge_current = (data[3] * 256 + data[2]) /10
#      json_string = json.dumps({'last_update' : time_now, 'static_max_voltage': max_voltage, 'target_charge_current': charge_current}, separators=(',', ':'))
      json_string = json.dumps({'last_update' : time_now, 'target_charge_current': charge_current}, separators=(',', ':'))
      return json_string
    if id == "0x140400":
      min_voltage = (data[7] * 256 + data[6]) /10
      discharge_current = (data[3] * 256 + data[2]) /10
#      json_string = json.dumps({'last_update' : time_now, 'static_min_voltage': min_voltage, 'target_discharge_current': discharge_current}, separators=(',', ':'))
      json_string = json.dumps({'last_update' : time_now, 'target_discharge_current': discharge_current}, separators=(',', ':'))
      return json_string
    if re.search("^0x1d11",id): # Cell stats
      cell_num = int(id[6:8],16)
      min_cell_voltage = (data[1] * 256 + data[0]) /1000
      max_cell_voltage = (data[3] * 256 + data[2]) /1000
      cell_t = data[4] - 40
      bypass_pwm = data[6] # 0-100%
      json_string = json.dumps({'last_update' : time_now, 'cellstat_cell_num': cell_num, 'cellstat_max_voltage': max_cell_voltage, 'cellstat_min_voltage': min_cell_voltage, 'cellstat_cell_temp': cell_t,'cellstat_cell_balancing': bypass_pwm }, separators=(',', ':'))
      return json_string

    return False
