#!/usr/bin/env python
"""
Pymodbus Server With Updating Thread
- Spoofs a BYD to GEN24 // Per Carlen
"""
import os
import asyncio

# --------------------------------------------------------------------------- #
# import the modbus libraries we need
# --------------------------------------------------------------------------- #
from pymodbus.version import version
from pymodbus.server import StartAsyncSerialServer
from pymodbus.device import ModbusDeviceIdentification
from pymodbus.datastore import ModbusSlaveContext, ModbusServerContext
from pymodbus.transaction import ModbusRtuFramer, ModbusAsciiFramer
from pymodbus.datastore import ModbusSequentialDataBlock, ModbusSparseDataBlock

#from twisted.internet.task import LoopingCall
import time
import json
import yaml
import paho.mqtt.client as paho
import copy


# --------------------------------------------------------------------------- #
# some global vars
# --------------------------------------------------------------------------- #
inverter_debug = 0
bms_timeout = 120
last_updated_soc = 0
reg_401 = {}
reg_1001 = {}

# --------------------------------------------------------------------------- #
# configure logging
# --------------------------------------------------------------------------- #
import logging
if inverter_debug:
    logging.basicConfig()
    log = logging.getLogger()
    log.setLevel(logging.DEBUG)


# ----------------------------------------------------------------------- #
# initialize your data store
# ----------------------------------------------------------------------- #
block = ModbusSparseDataBlock(
    {101: [0] * 68, 201: [0] * 13, 301: [0] * 24, 401: [0] * 20, 1001: [0] * 100, 12289: [0] * 768})
store = ModbusSlaveContext(di=block, co=block, hr=block, ir=block, unit=21)
context = ModbusServerContext(slaves=store, single=True)

def read_config_file():

    config_json = ""
    filename = "config.yaml"
    path = "/etc/batt2gen24/"
    if os.path.isfile(path+filename):
        filename = path + filename
    print("reading from:",filename)
    try:
      with open(filename,'r') as file:
        config_json = json.dumps(yaml.safe_load(file))
    except:
      print("ERROR: No config file - ",filename)
      exit(1)

    return config_json

def convert2unsignedint16(signed_value):
  if signed_value < 0:
    return (65535 + signed_value)
  else:
    return signed_value

def Str2Int32(s):
    a1 = ord(s[0:1])
    if len(s) == 2:
        a2 = ord(s[1:2])
    else:
        a2 = 0
    ret = a1*256 + a2
    return ret

# --------------------------------------------------------------------------- #
# thread to update the registers from mqtt data
# --------------------------------------------------------------------------- #
def update_registers():
    global context
    global bms_timeout
    global inverter_debug
    global logic_data
    global last_updated_soc

    slave_id = 0x00

    # Read the current stored values in the registers
    reg_101 = context[slave_id].getValues(4, 100, count=68)
    reg_201 = context[slave_id].getValues(4, 200, count=13)
    reg_301 = context[slave_id].getValues(3, 300, count=24)
    reg_401 = context[slave_id].getValues(4, 400, count=20)
    reg_1001 = context[slave_id].getValues(4, 1000, count=100)

    if reg_301[0] == 4: #FAULT, don't change this unless bms is restarted
      time_now = int(time.time())
      time_diff = int(time.time() - last_updated_soc)
      if time_diff > bms_timeout:
        print("Refusing to update when FAULT with old BMS data")
        return

    pr101 = [ Str2Int32("SI") , 1 ]
    pr103 = [ Str2Int32("BY"), Str2Int32("D"), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    pr119 = [ Str2Int32("BY"), Str2Int32("D "), Str2Int32("Ba"), Str2Int32("tt"), Str2Int32("er"), Str2Int32("y-"), Str2Int32("Bo"), Str2Int32("x "), Str2Int32("Pr"), Str2Int32("em"), Str2Int32("iu"), Str2Int32("m "), Str2Int32("HV"), 0, 0, 0]
    pr135 = [ Str2Int32("5."), Str2Int32("0"), 0, 0, 0, 0, 0, 0, Str2Int32("3."), Str2Int32("16"), 0, 0, 0, 0, 0, 0]
    pr151 = [ Str2Int32("Pe"), Str2Int32("ll"), Str2Int32("e"), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ]
    pr167 = [ 1, 0 ]

    p101 = pr101 + pr103 + pr119 + pr135 + pr151 + pr167

    p201 = [0, 0, 0, 0, 0, 0, 0, 53248, 10, 53248, 10, 0, 0]
    p301 = [0] * 24 #[0, 0, 128, 2000, 10000, 514, 0, 8352, 0, 0, 2058, 0, 60, 70, 0, 0, 0, 0, 0, 0, 0, 0, 240, 9900]
    p401 = [0]*20
    p1001 = [0]*100

    static_nominal_capacity = int(logic_data['static_nominal_capacity'])
    if static_nominal_capacity > 32000: # Gen24 can't handle big num
      static_nominal_capacity = 32000
    static_max_power = logic_data['static_max_power']
    static_max_voltage = logic_data['static_max_voltage']
    static_min_voltage = logic_data['static_min_voltage']
    p201[2] = static_nominal_capacity
    p201[3] = static_max_power
    p201[4] = static_max_power
    p201[5] = int(static_max_voltage * 10)
    p201[6] = int(static_min_voltage * 10)

    p301[0] = int(logic_data['status'])
    p301[2] = int(logic_data['mode'])
    p301[3] = int(logic_data['soc'] * 100)
    p301[4] = static_nominal_capacity
    p301[5] = int(static_nominal_capacity * logic_data['soc'] / 100)
    p301[6] = int(logic_data['target_discharge_power'])
    p301[7] = int(logic_data['target_charge_power'])
    if logic_data['status'] == 3:
      p301[8] = int(10 * logic_data['batt_voltage'])
      p301[9] = convert2unsignedint16(int(logic_data['batt_power']))
    else:
      p301[8] == 0
      p301[9] == 0
    p301[10] = int(10 * logic_data['batt_voltage'])
    p301[11] = p301[9]
    p301[12] = convert2unsignedint16(logic_data['cell_temp_min'] * 10) 
    p301[13] = convert2unsignedint16(logic_data['cell_temp_max'] * 10)
    p301[22] = convert2unsignedint16(logic_data['cell_temp_max'] * 10)
    p301[23] = int(logic_data['soh'] * 100)

    updated = False
    if p101 != reg_101:
      reg_101 = p101
      context[slave_id].setValues(3,100, reg_101)
      updated = True
    if p201 != reg_201:
      reg_201 = p201
      context[slave_id].setValues(3,200, reg_201)
      updated = True
    if p301 != reg_301:
      reg_301 = p301
      context[slave_id].setValues(3,300, reg_301)
      updated = True

    return updated

#    exit(1)


# --------------------------------------------------------------------------- #
# thread to check registers for inverter writes
# --------------------------------------------------------------------------- #
async def check_registers(context):
  global mqtt_client1
  global logic_data
  global bms_timeout
  global inverter_debug
  global reg_401
  global reg_1001
  global last_updated_soc

  while 1:
    time_now = int(time.time())
    time_diff = int(time.time() - last_updated_soc)
    if inverter_debug: print("Check register - start - diff:",time_diff)

    #context = a[0]
    slave_id = 0x00

    # Read the current stored values in the registers
    values_101 = context[slave_id].getValues(4, 100, count=68)
    values_201 = context[slave_id].getValues(4, 200, count=13)
    values_301 = context[slave_id].getValues(4, 300, count=24)
    values_401 = context[slave_id].getValues(4, 400, count=20)
    values_1001 = context[slave_id].getValues(4, 1000, count=100)
    if inverter_debug:
      print("101:",values_101)
      print("201:",values_201)
      print("301:",values_301)
      print("401:",values_401)
      print("1001:",values_1001)

    if time_diff > bms_timeout:
      print("Old BMS data, setting status to INACTIVE. Last updated (secs)", time_diff)
      values_301[0] = 1 #INACTIVE
      context[slave_id].setValues(3,300, values_301)

    if values_401 == reg_401 and values_1001 == reg_1001:
      await asyncio.sleep(1)
    else:

      reg_401 = copy.deepcopy(values_401)
      reg_1001 = copy.deepcopy(values_1001)

      time_now = str(int(time.time()))
      if inverter_debug: print("Publishing registers",time_now)
      json_string = json.dumps({'last_update' : time_now, 'r401': reg_401, 'r1001': reg_1001}, separators=(',', ':'))
      rc = publish(mqtt_client1,json_string,"inverter/writes")
      await asyncio.sleep(1)



def mqtt_on_publish(mqtt_client,userdata,result): 
    global inverter_debug
    if inverter_debug: print("data published:",result)
    pass

def mqtt_on_message(mqtt_client,userdata, message,tmp=None):
    global inverter_debug
    global logic_data
    global last_updated_soc

    if inverter_debug:
      print(" Received message " + str(message.payload)
        + " on topic '" + message.topic
        + "' with QoS " + str(message.qos))


    if message.topic == "inverter/data":
      if inverter_debug: print("Got mqtt update")
      json_data = json.loads(message.payload)
      last_updated_soc = json_data['last_updated_soc']
      if type(json_data) is dict or type(json_data) is list:
        logic_data = json_data
        if inverter_debug: print("data received:",logic_data)
        rc = update_registers()
        if rc:
          if inverter_debug: print("Registers updated from mqtt data")
        else:
          if inverter_debug: print("No need to update registers from mqtt data")


def mqtt_on_subscribe(client, userdata, mid, granted_qos):
    #print(" Received message " + str(client)
    #    + "' with QoS " + str(granted_qos))
    pass

def mqtt_on_connect(client, userdata, flags, rc):
    if rc != 0:
      print("MQTT connection problem")
      client.connected_flag=False
    else:
      print("MQTT client connected:" + str(client))
      client.connected_flag=True

def publish(client,msg_string,topic):
    global inverter_debug
    #print("Publishing in",topic,msg_string)
    ret = client.publish(topic,msg_string) 


def run_updating_server(class_InverterHandler):

    global InverterHandler
    global inverter_type
    global logic_data
    global mqtt_client1
    global mqtt_client2
    global reg_101
    global inverter_debug
    global bms_timeout
    global context

    InverterHandler = class_InverterHandler
    inverter_debug = InverterHandler.config['inverter_debug']
    print("\nStarting inverter module for Gen24")
    bms_timeout = InverterHandler.config['bms_timeout']
    inverter_debug = InverterHandler.config['inverter_debug']
    inverter_type = InverterHandler.config['inverter_type']
    inverter_interface = InverterHandler.config['inverter_interface']
    mqtt_broker = InverterHandler.config['mqtt_broker']
    mqtt_port = InverterHandler.config['mqtt_port']
    mqtt_username = InverterHandler.config['mqtt_username']
    mqtt_password = InverterHandler.config['mqtt_password']

    # this mqtt client will publish
    mqtt_client1 = paho.Client(client_id="inverter_pub",transport="tcp",protocol=paho.MQTTv311,clean_session=True)  
    mqtt_client1.on_publish = mqtt_on_publish
    mqtt_client1.on_connect = mqtt_on_connect
    mqtt_client1.username_pw_set(mqtt_username, mqtt_password)
    mqtt_client1.connect(mqtt_broker,mqtt_port,keepalive=60)
    mqtt_client1.loop_start()

    # this mqtt client will subscribe
    mqtt_client2 = paho.Client(client_id="inverter_sub",transport="tcp",protocol=paho.MQTTv311,clean_session=True)  
    mqtt_client2.on_subscribe = mqtt_on_subscribe
    mqtt_client2.on_connect = mqtt_on_connect
    mqtt_client2.on_message = mqtt_on_message
    mqtt_client2.username_pw_set(mqtt_username, mqtt_password)
    mqtt_client2.connect(mqtt_broker,mqtt_port,keepalive=60)
    mqtt_client2.subscribe("inverter/data",2)
    mqtt_client2.loop_start()

    # ----------------------------------------------------------------------- # 
    # initialize the server information
    # ----------------------------------------------------------------------- # 
    identity = ModbusDeviceIdentification()
    identity.VendorName = 'pymodbus'
    identity.ProductCode = 'PM'
    identity.VendorUrl = 'http://github.com/riptideio/pymodbus/'
    identity.ProductName = 'pymodbus Server'
    identity.ModelName = 'pymodbus Server'
    identity.MajorMinorRevision = version.short()

    logic_data = {}
    must_have_keys = ['static_nominal_capacity','last_updated_soc']
    while not all(k in logic_data for k in must_have_keys):
      if inverter_debug: print("No mqtt data yet")
      time.sleep(1)
    print("Starting modbus server")
    asyncio.run(run_async_server(context, identity, inverter_interface), debug=True)
    print("Server ended")
    exit(1)

async def run_async_server(context, identity, inverter_interface):
    print("Register task for inverter write check")
    asyncio.create_task(check_registers(context))
    await StartAsyncSerialServer(context=context, framer=ModbusRtuFramer, identity=identity, port=inverter_interface, timeout=.005, baudrate=9600)

if __name__ == '__main__':
    run_updating_server()

