#!/usr/bin/env python

# 2023 Per Carlen
#
# This is a handler for Juntek Shunt (sort of BMS)
# Calls specific BMS-module to decode message and stores data in generic dictionaries
# Publishes bms data
#  test sub with: $ mosquitto_sub -h localhost -t bms/stat -u "batt2gen24" -P "batt2gen24_pass"
# Subscribes on control commands that will be sent to vms
#  test pub with: $ mosquitto_pub -h localhost -m "close_contactor" -t "bms/control" -u "batt2gen24" -P "batt2gen24_pass"
#
#

import gc

import hashlib
import copy
import os
import socket

from pymodbus.client import ModbusTcpClient as ModbusClient
import time
import asyncio
import json
import yaml
import re
import paho.mqtt.client as paho
from typing import List
import serial
from datetime import datetime


# global vars
bms_type = ""
bms_update_interval = 60

bms_static_dict = {}
bms_stat_dict = {}
bms_cellstat_list = []
bms_target_dict = {}
bms_specific_dict = {}

last_bms_static_dict = {}
last_bms_stat_dict = {}
last_bms_cellstat_list = []
last_bms_target_dict = {}
last_bms_specific_dict = {}

fake_soc_enabled=0
fake_soc_zero=0
fake_soc_hundred=100

bms_debug = 0

def read_bms_config_file(config):

    global bms_type

    config_json = ""
    filename = "bms_config_" + config['bms_type'] + ".yaml"
    path = "/etc/batt2gen24/"
    if os.path.isfile(path+filename):
        filename = path + filename
    print("reading from:",filename)
    try:
      with open(filename,'r') as file:
        config_json = json.dumps(yaml.safe_load(file))
    except:
      pass

    return config_json



def update_bms_dict(time_now,json_dict):

    global bms_specific_dict
    global bms_static_dict
    global bms_stat_dict
    global bms_cellstat_list 
    global bms_target_dict
    global bms_debug
    global fake_soc_enabled
    global fake_soc_zero
    global fake_soc_hundred


    for key in json_dict:
      #print("-",key,"->",json_dict[key])
      if re.search("^stat_",key):
        temp = bms_stat_dict
      elif re.search("^specific_",key):
        temp = bms_specific_dict
      elif re.search("^static_",key):
        temp = bms_static_dict
      elif re.search("^target_",key):
        temp = bms_target_dict
      elif re.search("^cellstat_",key):
        temp = bms_cellstat_list
      else:
        temp = ""
      if type(temp) is dict or type(temp) is list:
        if re.search("^cellstat_",key): # this will be a list of dictionaries
          cell_num = json_dict['cellstat_cell_num']
          hit = -1
          for index,item in enumerate(temp):
            if item['cellstat_cell_num'] == cell_num:
              hit = index
          if hit > -1:  # Update data for this cell_num
            temp[hit].update(json_dict)
          else:
            temp.append(json_dict)  # New cell_num, add data
        else: # Easier if not a list...just add/change 
          temp[key] = json_dict[key]
          if key == 'stat_soc':
             temp['last_updated_soc'] = time_now
             if fake_soc_enabled:
                 json_dict[key]= (json_dict[key]-fake_soc_zero) / (fake_soc_hundred - fake_soc_zero) * 100
          temp['last_updated'] = time_now

        if re.search("^stat_",key):
          bms_stat_dict = temp
        elif re.search("^static_",key):
          bms_static_dict = temp
        elif re.search("^specific_",key):
          bms_specific_dict = temp
        elif re.search("^target_",key):
          bms_target_dict = temp
        elif re.search("^cellstat_",key):
          bms_cellstat_list = temp
        else:
          print("What kind of data is this:",temp)


def update_bms_data(json_string):
    if json_string:
          json_data = json.loads(json_string)
          time_now = time.time()

          if type(json_data) is list:
            for json_dict in json_data:
              update_bms_dict(time_now,json_dict)
          else:
            update_bms_dict(time_now,json_data)

def mqtt_on_publish(mqtt_client,userdata,result): 
    #print("data published:",result)
    pass

def mqtt_on_message(mqtt_client,userdata, message,tmp=None):
    global bus
    global bms_debug

    if bms_debug:
        print(" Received message " + str(message.payload)
        + " on topic '" + message.topic
        + "' with QoS " + str(message.qos))
    if message.topic == "bms/control":
      if bms_type == "tesla_model3":
        rc = tesla_model3_send(bus,message.payload.decode(),bms_debug)

def mqtt_on_subscribe(client, userdata, mid, granted_qos):
    pass

def mqtt_on_connect(client, userdata, flags, rc):
    if rc != 0:
      print("MQTT connection problem")
      client.connected_flag=False
    else:
      print("MQTT client connected:" + str(client))
      client.connected_flag=True

def publish(client,pub_var,topic):
    json_string = json.dumps(pub_var)
    ret = client.publish(topic,json_string) 
    if bms_debug: print("publishing in:",topic)

# publish if there are changes, besides "last_updated"
def compare_and_publish(client,new_var,topic,old_var):
  global bms_debug

  new_json_str = json.dumps(new_var)
  old_json_str = json.dumps(old_var)
  new_json = re.sub(r"\"last_updated.*,", "", new_json_str)
  old_json = re.sub(r"\"last_updated.*,", "", old_json_str)
  sha_1_new = hashlib.sha1(new_json.encode('ascii')).hexdigest()
  sha_1_old = hashlib.sha1(old_json.encode('ascii')).hexdigest()
  if sha_1_new != sha_1_old:
    if bms_debug: print("publishing a diff in:",topic)
    publish(client,new_var,topic)
    return True
  return False


def run_updating_server(class_BMSHandler,bms_comms_module):

    global bms

    global bms_debug
    global mqtt_client1
    global mqtt_client2

    global bms_type
    global bus

    global bms_specific_dict
    global bms_static_dict
    global bms_stat_dict
    global bms_cellstat_list 
    global bms_target_dict

    global fake_soc_enabled
    global fake_soc_zero
    global fake_soc_hundred

    global smarthome

    bms = bms_comms_module
    smarthome = False

    BMSHandler = class_BMSHandler
    config = BMSHandler.config
    bms_debug = config['bms_debug']

    print("\nStarting bms_handler")
    if config:
      bms_type = config['bms_type']
      bms_debug = config['bms_debug']
      bms_update_interval = int(config['bms_update_interval'])
      try:
          fake_soc_enabled = int(config['fake_soc_enabled'])
          fake_soc_zero = int(config['fake_soc_zero'])
          fake_soc_hundred = int(config['fake_soc_hundred'])
      except KeyError:
        print("fake_soc_(enable/zero/hundred) values missing in config file")
      try:
          mqtt_smarthome_broker = config['mqtt_smarthome_broker']
          mqtt_smarthome_port = config['mqtt_smarthome_port']
          mqtt_smarthome_username = config['mqtt_smarthome_username']
          mqtt_smarthome_password = config['mqtt_smarthome_password']
          smarthome = True
      except KeyError:
          print("no Smarthome MQTT defined")
      mqtt_broker = config['mqtt_broker']
      mqtt_port = config['mqtt_port']
      mqtt_username = config['mqtt_username']
      mqtt_password = config['mqtt_password']
    else:
      exit(1)

    print("Reading specific bms-config from file")
    bms_specific_config = read_bms_config_file(config)
    if bms_specific_config:
      update_bms_data(bms_specific_config)
    next_run = 0

    # this mqtt client will publish
    mqtt_client1 = paho.Client(client_id="bms_handler_pub",transport="tcp",protocol=paho.MQTTv311,clean_session=True)  
    mqtt_client1.on_publish = mqtt_on_publish
    mqtt_client1.on_connect = mqtt_on_connect
    mqtt_client1.username_pw_set(mqtt_username, mqtt_password)
    mqtt_client1.connect(mqtt_broker,mqtt_port,keepalive=60)
    mqtt_client1.loop_start()

    # this mqtt client will subscribe
    mqtt_client2 = paho.Client(client_id="bms_handler_sub",transport="tcp",protocol=paho.MQTTv311,clean_session=True)  
    mqtt_client2.on_subscribe = mqtt_on_subscribe
    mqtt_client2.on_connect = mqtt_on_connect
    mqtt_client2.on_message = mqtt_on_message
    mqtt_client2.username_pw_set(mqtt_username, mqtt_password)
    mqtt_client2.connect(mqtt_broker,mqtt_port,keepalive=60)
    mqtt_client2.subscribe("bms/control",2)
    mqtt_client2.loop_start()

    mqtt_client_smarthome = ""
    if smarthome:
        mqtt_client_smarthome = paho.Client(client_id=socket.gethostname(), transport="tcp",
                                            protocol=paho.MQTTv311,
                                            clean_session=True)
        mqtt_client_smarthome.on_publish = mqtt_on_publish
        mqtt_client_smarthome.on_connect = mqtt_on_connect
        mqtt_client_smarthome.username_pw_set(mqtt_smarthome_username, mqtt_smarthome_password)
        mqtt_client_smarthome.connect(mqtt_smarthome_broker, mqtt_smarthome_port, keepalive=60)
        mqtt_client_smarthome.loop_start()

    print("Starting")
    asyncio.run(run_async_server(BMSHandler.config,json.loads(bms_specific_config)), debug=False)
    print("Server ended")
    exit(1)

async def run_async_server(config,bms_specific_config):
    print("Register task for listening on modbus")
    asyncio.create_task(run_modbus_listener(config,bms_specific_config))
    while True:
      print("will start updating tasks")
      await asyncio.sleep(1)
      await StartUpdating(config)


def shift(reg,v):

  i = 1
  while i < len(reg):
    reg[i-1] = reg[i]
    i += 1
  reg[len(reg)-1] = v
  return reg

def print_char(b):
  global line

  ch = ord(b.decode("ascii"))
  if (ch>31 and ch<128):
    line += b.decode("ascii")
    #print(b.decode("ascii"), end = '')
  if ch==2:
    line += "<STX>"
  if ch==3:
    line += "<ETX>"
  if ch==4:
    line += "<EOT>"
  if ch==13:
    print(line)
    check_line(line)
    line = ""

def print_rr(lbl,rr):
  print(lbl + ":",end='')
  for x in rr.registers:
    s = x.to_bytes(2,"big").hex()
    print(s + " (" + str(x) + ")",end='')
  print("")


async def run_modbus_listener(config,bms_specific_config):

  batt_amp_hours_full = int(bms_specific_config['stat_soh']) * int(int(bms_specific_config['static_nominal_capacity']) / int(bms_specific_config['static_max_cell_voltage'])) / 100

  ser = serial.Serial(
    port=config['bms_interface_name'],
    baudrate=config['bms_interface_bitrate'],
    parity=serial.PARITY_NONE,
    stopbits=serial.STOPBITS_ONE,
    bytesize=serial.EIGHTBITS
  )

  if config['bms_debug']: print("RS485 mode:",bms_specific_config['rs485_mode'])
  ser.isOpen()
  await asyncio.sleep(1)

  while True:
    # Empty serial buffer
    while ser.inWaiting() > 0:
      out = ser.read(1)
      time.sleep(0.001)
    if bms_specific_config['rs485_mode'] == "listen":
      if config['bms_debug']: print("Waiting for header on RS485")
      header = False
      header_done = False
      i = 0
      while not header_done:
        out = ser.read(1)
        if ord(out) == 170: 
          i = 0
          header = True 
        #print(i,":",ord(out))
        if header and i == 4:
          if ord(out) == 176:
            header_done = True
        i = i + 1
        await asyncio.sleep(0.005)

    if bms_specific_config['rs485_mode'] == "master":
      date_now = datetime.today() #.strftime('%Y%m%d %h:%M:%s')
      print("Sending request",date_now)
      s = b'\xaa\x04\x01\x01\xb0'
      ser.write(s)

    packet_done = False
    i = 0
    ba = []
    prev = 0
    while not packet_done:
      out = ser.read(1)
      ba.append(ord(out))
      temp = prev * 256 + ord(out)
      prev = ord(out)
      #print(i,":",ord(out),"-",temp)
      i = i + 1
      if i > 28:
        packet_done = True
    time.sleep(0.005)
    json_string = bms.decode(ba,config['bms_debug'],batt_amp_hours_full,bms_specific_config)
    if json_string:
      update_bms_data(json_string)
    await asyncio.sleep(1)



async def StartUpdating(config):

    global mqtt_client1
    global mqtt_client2

    global last_bms_static_dict
    global last_bms_stat_dict 
    global last_bms_target_dict 
    global last_bms_specific_dict 
    global last_bms_cellstat_list

    global smarthome

    global bms


    seconds = 0
    try:
            while True:
                # Check if values have changed, in that case - publish right away
                rc = compare_and_publish(mqtt_client1,bms_static_dict,"bms/static",last_bms_static_dict)
                rc = compare_and_publish(mqtt_client1,bms_stat_dict,"bms/stat",last_bms_stat_dict)
                rc = compare_and_publish(mqtt_client1,bms_target_dict,"bms/target",last_bms_target_dict)
                rc = compare_and_publish(mqtt_client1,bms_specific_dict,"bms/specific",last_bms_specific_dict)
                rc = compare_and_publish(mqtt_client1,bms_cellstat_list,"bms/cellstat",last_bms_cellstat_list)

                if seconds == config['bms_update_interval']:
                  #if config['bms_debug']: print("Publishing,  garbage collector:",str(len( gc.get_objects() )) )
                  publish(mqtt_client1,bms_static_dict,"bms/static")
                  publish(mqtt_client1,bms_stat_dict,"bms/stat")
                  publish(mqtt_client1,bms_target_dict,"bms/target")
                  publish(mqtt_client1,bms_specific_dict,"bms/specific")
                  publish(mqtt_client1,bms_cellstat_list,"bms/cellstat")

                  if smarthome:
                      data_to_public = {"bms/static": bms_static_dict,
                                        "bms/stat": bms_stat_dict,
                                        "bms/target": bms_target_dict,
                                        "bms/specific": bms_specific_dict,
                                        "bms/cellstat": bms_cellstat_list}
                      json_string = json.dumps(
                          data_to_public,
                          separators=(',', ':'))
                      mqtt_client_smarthome.publish('tele/batt2gen24_'+socket.gethostname()+'/STATE', json_string)

                  last_bms_static_dict = copy.deepcopy(bms_static_dict)
                  last_bms_stat_dict = copy.deepcopy(bms_stat_dict)
                  last_bms_target_dict = copy.deepcopy(bms_target_dict)
                  last_bms_specific_dict = copy.deepcopy(bms_specific_dict)
                  last_bms_cellstat_list = copy.deepcopy(bms_cellstat_list)

                  seconds = 0
                await asyncio.sleep(2)
                seconds += 2



    except KeyboardInterrupt:
            # Wait for last message to arrive
            await reader.get_message()
            global debug
            print("Done!")

            # Clean-up
            notifier.stop()
            mqtt_client1.disconnect()
            mqtt_client2.disconnect()
            pass  # exit normally


if __name__ == "__main__":
    asyncio.run(run_updating_server())

